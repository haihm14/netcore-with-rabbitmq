﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQReiverCoreAPI.Models;
using RabbitMQReiverCoreAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQReiverCoreAPI.RebbitMQ
{
    public class EventBusRabbitMQ : IDisposable
    {
        private readonly IRabbitMQPersistentConnection _persistentConnection;
        private IModel _consumerChannel;
        private string _queueName;
        UserService _userService = new UserService();

        public EventBusRabbitMQ(IRabbitMQPersistentConnection persistentConnection, string queueName = null)
        {
            _persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
            _queueName = queueName;
        }

        public IModel CreateConsumerChannel()
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var channel = _persistentConnection.CreateModel();
            channel.QueueDeclare(queue: _queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            //consumer.Received += async (model, e) =>
            //{
            //    var eventName = e.RoutingKey;
            //    var message = Encoding.UTF8.GetString(e.Body);
            //    channel.BasicAck(e.DeliveryTag, multiple: false);
            //};

            //Create event when something receive
            consumer.Received += ReceivedEvent;



            channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);
            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
            };
            return channel;
        }

        private void ReceivedEvent(object sender, BasicDeliverEventArgs e)
        {
            Console.WriteLine(sender);
            Console.WriteLine(e.ConsumerTag);
            //CHannel 01
            if (e.RoutingKey == "userInsertMsgQ")
            {
                Console.WriteLine("*****************************************************************");
                var message = Encoding.UTF8.GetString(e.Body);
                List<User> userList = JsonConvert.DeserializeObject<List<User>>(message);
                // UserSaveFeedback saveFeedback = _userService.InsertUsers(userList);
                foreach (var item in userList)
                {

                Console.WriteLine("List user here: ");
                    Console.WriteLine(item.FirstName + " - " + item.EmailAddress);
                }
                Console.WriteLine("##################################################################");
                // PublishUserSaveFeedback("userInsertMsgQ_feedback", saveFeedback,e.BasicProperties.Headers);
            }

            //CHannel 02
            if (e.RoutingKey == "messageInput")
            {
                Console.WriteLine("*****************************************************************");
                //Implementation here
                var message = Encoding.UTF8.GetString(e.Body);

                Console.WriteLine("Message user input");
                if(message.Length > 0)
                {
                    Console.WriteLine("Receive: " + message);
                }
                Console.WriteLine("##################################################################");
            }

            // Chanel 03
            if (e.RoutingKey == "topic.bombay.queue")
            {
                Console.WriteLine("*****************************************************************");
                //Implementation here
                var message = Encoding.UTF8.GetString(e.Body);

                Console.WriteLine("topic.bombay.queue: ");
                if (message.Length > 0)
                {
                    Console.WriteLine("Receive: " + message);
                }
                Console.WriteLine("##################################################################");
            }

            // Chanel 04
            if (e.RoutingKey == "topic.delhi.queue")
            {
                Console.WriteLine("*****************************************************************");
                //Implementation here
                var message = Encoding.UTF8.GetString(e.Body);

                Console.WriteLine("topic.delhi.queue: ");
                if (message.Length > 0)
                {
                    Console.WriteLine("Receive: " + message);
                }
                Console.WriteLine("##################################################################");
            }

            // Chanel 05
            if (e.RoutingKey == "topic.bombay.toRed")
            {
                Console.WriteLine("*****************************************************************");
                //Implementation here
                var message = Encoding.UTF8.GetString(e.Body);

                Console.WriteLine("topic.bombay.toRed: ");
                if (message.Length > 0)
                {
                    Console.WriteLine("Receive: " + message);
                }
                Console.WriteLine("##################################################################");
            }

            // Chanel 03
            if (e.RoutingKey == "city.district.wards")
            {
                Console.WriteLine("*****************************************************************");
                //Implementation here
                var message = Encoding.UTF8.GetString(e.Body);

                Console.WriteLine("topic.queue.red: ");
                if (message.Length > 0)
                {
                    Console.WriteLine("Receive: " + message );
                }
                Console.WriteLine("##################################################################");
            }

            // Chanel 03
            if (e.RoutingKey == "NOcity.district.NOwards")
            {
                Console.WriteLine("*****************************************************************");
                //Implementation here
                var message = Encoding.UTF8.GetString(e.Body);

                Console.WriteLine("topic.queue.blue: ");
                if (message.Length > 0)
                {
                    Console.WriteLine("Receive: " + message);
                }
                Console.WriteLine("##################################################################");
            }
        }

        public void PublishUserSaveFeedback(string _queueName, UserSaveFeedback publishModel, IDictionary<string, object> headers)
        {           

            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {

                channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
                var message = JsonConvert.SerializeObject(publishModel);
                var body = Encoding.UTF8.GetBytes(message);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;
                properties.DeliveryMode = 2;
                properties.Headers = headers;
                // properties.Expiration = "36000000";
                //properties.ContentType = "text/plain";

                channel.ConfirmSelect();
                channel.BasicPublish(exchange: "", routingKey: _queueName, mandatory: true, basicProperties: properties, body: body);
                channel.WaitForConfirmsOrDie();

                channel.BasicAcks += (sender, eventArgs) =>
                {
                    Console.WriteLine("Sent RabbitMQ");
                    //implement ack handle
                };
                channel.ConfirmSelect();
            }
        }

        public void Dispose()
        {
            if (_consumerChannel != null)
            {
                _consumerChannel.Dispose();
            }
        }
    }
}
