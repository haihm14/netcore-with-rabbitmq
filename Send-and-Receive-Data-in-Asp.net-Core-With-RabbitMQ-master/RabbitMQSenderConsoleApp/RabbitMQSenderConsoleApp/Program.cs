﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQSenderConsoleApp
{
    class Program
    {

        //packege manager console > Install-Package RabbitMQ.Client
        //packege manager console > Install-Package Newtonsoft.json

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost", UserName = "guest", Password = "guest" };

            Console.WriteLine("Sender application!");
            Console.WriteLine("press 'A' to send list user");
            Console.WriteLine("press 'B' to send message");
            Console.WriteLine("press 'C' to send message to direct");
            Console.WriteLine("press 'D' to send message to topic");

            ConsoleKey key = ConsoleKey.Escape;
            while (key != ConsoleKey.Enter)
            {
                Console.Write("\nInput your select!: ");
                key = Console.ReadKey().Key;
                switch (key)
                {
                    case ConsoleKey.A:
                        SendingData(factory);
                        break;
                    case ConsoleKey.B:
                        SendingMessageInput(factory);
                        break;
                    case ConsoleKey.C:
                        SendingDirectExchange(factory);
                        break;
                    case ConsoleKey.D:
                        SendTopicExchange(factory);
                        break;
                    default:
                        key = ConsoleKey.Enter;
                        break;
                }
            }
            Console.WriteLine("Exit");
            Console.ReadKey();


            //-------------------------  Sending Data --------------------------------------------------------------------------------------
            //-------------------------  Receiving feedback ---------------------------------------------------------------------------------
            #region Feedback Received part
            //using (var channel = connection.CreateModel())
            //{
            //    channel.QueueDeclare(queue: "userInsertMsgQ_feedback",
            //                       durable: false,
            //                       exclusive: false,
            //                       autoDelete: false,
            //                       arguments: null);

            //    var consumer = new EventingBasicConsumer(channel);
            //    consumer.Received += (model, ea) =>
            //    {
            //        IDictionary<string, object> headers=ea.BasicProperties.Headers; // get headers from Received msg

            //        foreach (KeyValuePair<string, object> header in headers)
            //        {                      
            //            if (senderUniqueId == Encoding.UTF8.GetString((byte[])header.Value))// Get feedback message only for me
            //            {
            //                var body = ea.Body;
            //               var message = Encoding.UTF8.GetString(body);
            //                UserSaveFeedback feedback = JsonConvert.DeserializeObject<UserSaveFeedback>(message);
            //                Console.WriteLine("[x] Feedback received ... ");
            //                Console.WriteLine("[x] Success count {0} and failed count {1}", feedback.successCount, feedback.failedCount);
            //           }
            //        }
            //    };

            //    channel.BasicConsume(queue: "userInsertMsgQ_feedback",
            //                         autoAck: true,
            //                         consumer: consumer);

            //    Console.WriteLine("Waiting for feedback. Press [enter] to exit.");
            //    Console.ReadLine();
            //}
            #endregion
        }

        static void SendingData(ConnectionFactory factory)
        {
            string senderUniqueId = "userInsertMsgQ";
            var connection = factory.CreateConnection();

            #region Sending Data
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "userInsertMsgQ", durable: false, exclusive: false, autoDelete: false, arguments: null);
                // create serialize object to send
                UserService _userService = new UserService();
                List<User> objeUserList = _userService.GetAllUsersToSend();
                string message = JsonConvert.SerializeObject(objeUserList);

                var body = Encoding.UTF8.GetBytes(message);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;
                properties.DeliveryMode = 2;
                properties.Headers = new Dictionary<string, object>();
                properties.Headers.Add("senderUniqueId", senderUniqueId);//optional unique sender details in receiver side              
                // properties.Expiration = "36000000";
                // properties.ContentType = "text/plain";

                // channel.ConfirmSelect();

                channel.BasicPublish(exchange: "",
                                     routingKey: "userInsertMsgQ",
                                     basicProperties: properties,
                                     body: body);
                // channel.WaitForConfirmsOrDie();

                //channel.BasicAcks += (sender, eventArgs) =>
                //{
                //    Console.WriteLine("Sent RabbitMQ");
                //    //implement ack handle
                //};
                // channel.ConfirmSelect();
            }
            #endregion
        }

        static void SendingMessageInput(ConnectionFactory factory)
        {
            var connection = factory.CreateConnection();

            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "messageInput", durable: false, exclusive: false, autoDelete: false, arguments: null);
                Console.Write("\nInput Message: ");
                string messageInput = Console.ReadLine();

                var body = Encoding.UTF8.GetBytes(messageInput);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                                     routingKey: "messageInput",
                                     basicProperties: properties,
                                     body: body);
            }
        }

        static void SendingDirectExchange(ConnectionFactory factory)
        {
            var connection = factory.CreateConnection();

            using (var channel = connection.CreateModel())
            {
                Console.Write("\nInput message send to Blue: ");
                string messageInput = Console.ReadLine();
                var body = Encoding.UTF8.GetBytes(messageInput);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "request.exchange",
                                     routingKey: "directexchange_key_red",
                                     basicProperties: null,
                                     body: body);
                // Send to red
                Console.Write("\nInput message send to Red: ");
                messageInput = Console.ReadLine();
                body = Encoding.UTF8.GetBytes(messageInput);

                channel.BasicPublish(exchange: "request.exchange",
                                     routingKey: "directexchange_key_blue",
                                     basicProperties: null,
                                     body: body);
            }
        }

        static void SendTopicExchange(ConnectionFactory factory)
        {
            var connection = factory.CreateConnection();

            using (var channel = connection.CreateModel())
            {
                 // city.district.wards


                Console.Write("\n Send to queue distrinct with routing key 'city.district.wards' - send all: ");
                string messageInput = Console.ReadLine();
                var body = Encoding.UTF8.GetBytes(messageInput);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "topic.exchange",
                                     routingKey: "city.district.wards",
                                     basicProperties: null,
                                     body: body);

                // Send to red
                Console.Write("\n Send to queue distrinct with routing key 'NOwards.district.NOwards': ");
                messageInput = Console.ReadLine();
                body = Encoding.UTF8.GetBytes(messageInput);

                channel.BasicPublish(exchange: "topic.exchange",
                                     routingKey: "NOcity.district.NOwards",
                                     basicProperties: null,
                                     body: body);
            }
        }

    }
}
